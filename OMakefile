.PHONY: all install clean nuke buildenv ctan ctan-root tds tds-hier usage doc testenv

name		= rst

###############################################################################
# SOURCE
###############################################################################

doc_dir		= ./doc
src_dir		= ./src
misc_dir	= ./misc

## input
lua_src[]	= $(glob $(src_dir)/rst_*.lua)
tex_src		= $(src_dir)/t-rst.mkiv
script_src	= $(src_dir)/mtx-t-rst.lua
ifc_src		= $(misc_dir)/t-rst.xml

meta_files[]	= README.rst COPYING
build_files[]	= OMakeroot OMakefile $(doc_dir)/OMakefile
doc_src[]	= $(filter-out OMakefile, $(glob $(doc_dir)/*))

###############################################################################
# DIRECTORIES
###############################################################################

## The CTAN guys prefer the zipball to extract to a source directory with a
## flat structure containing all the files, and an accompanying TDS zipball
## at the root.
build_root	= ./build
doc_tree	= $(build_root)/doc/context/third/$(name)
tex_tree	= $(build_root)/tex/context/third/$(name)
ifc_tree	= $(build_root)/tex/context/interface/third
src_tree	= $(build_root)/source/context/$(name)/
script_tree	= $(build_root)/scripts/context/lua/third/$(name)
ctan_root	= $(build_root)/$(name)
doc_tmpdir	= $(build_root)/doc-tmp
tds_hier[] 	= $(doc_tree) $(tex_tree) $(script_tree) $(ifc_tree) $(src_tree)
tds_dirnames[]	= doc scripts tex
tds_dirs[]	= $(addprefix $(build_root)/,$(tds_dirnames))
test_dir	= ./testing

dirs[]		= $(build_root) $(ctan_root) $(tds_hier)

###############################################################################
# GENERATED
###############################################################################

ctan_zipfile	= $(name).zip
tds_zipfile	= $(name).tds.zip

manual_name	= manual
manual_pdf	= $(doc_tmpdir)/$(manual_name).pdf

ctan_zip	= $(build_root)/$(ctan_zipfile)
tds_zip		= $(build_root)/$(tds_zipfile)
pkgs		= $(ctan_zip) $(tds_zip)

###############################################################################
# TARGETS
###############################################################################

section
	OMakeFlags(-s)
	usage:
		echo "****************************** $(name) ******************************"
		echo
		echo " $(name) is distributed under the 2-clause BSD license. See the file"
		echo " COPYING in the root tree of this repository for details."
		echo
		echo "defined targets:"
		echo "    ctan      package for CTAN"
		echo "    tds       package as TDS directory hierarchy"
		echo
		echo "    doc       create module manual"
		echo
		echo "    testenv   create development environment in $(test_dir)"
		echo
		echo "    clean     remove temporary files"
		echo "    nuke      remove all created files"
		echo

ctan-root:
	mkdir(-p $(ctan_root))

tds-hier:
	#printvln(:: $(tds_hier))
	mkdir(-p $(tds_hier))

buildenv: ctan-root tds-hier

testenv:
	echo ":: creating test environment in $(test_dir)"
	mkdir(-p $(test_dir))
	## beware the foreach() syntax trap:
	##	http://lists.metaprl.org/pipermail/omake/2013-July/002256.html
	foreach(file => ..., $(glob $(src_dir)/*))
		symlink($(file), $(test_dir)/$(basename $(file)))
	foreach(file => ..., $(filter-out %OMakefile %.omc, $(glob $(doc_dir)/*)))
		symlink($(file), $(test_dir)/$(basename $(file)))
	foreach(file => ..., $(glob $(misc_dir)/*))
		symlink($(file), $(test_dir)/$(basename $(file)))

## nuke -- remove the entire build directory and packaged files
nuke:
	$(rm -rf -- $(build_root) $(pkgs) $(test_dir))

## clean -- leave the build directory, documentation and tarballs intact
clean: .PHONY/$(doc_tmpdir)/clean
	$(rm -rf -- $(tds_dirs) $(ctan_root))

## tds -- package according to TeX directory structure
tds: $(tds_zip)

## ctan -- package for CTAN upload
ctan: $(ctan_zip)

## PDF documentation
doc: .PHONY/$(doc_tmpdir)/manual

$(tds_zip): doc tds-hier
	$(echo $(name)> packaging as $@ with TDS directories)
	cp($(lua_src)     $(tex_tree))
	cp($(tex_src)     $(tex_tree))
	cp($(script_src)  $(script_tree))
	cp($(doc_src)     $(doc_tree))
	cp($(manual_pdf)  $(doc_tree))
	cp($(ifc_src)     $(ifc_tree))
	cp($(meta_files)  $(src_tree))
	cp($(build_files) $(src_tree))
	cd $(build_root) && zip -9 -r ./$(tds_zipfile) $(tds_dirnames)

$(ctan_zip): ctan-root $(tds_zip)
	$(echo $(name)> packaging as $@ for CTAN upload)
	cp($(lua_src)    $(ctan_root))
	cp($(script_src) $(ctan_root))
	cp($(tex_src)    $(ctan_root))
	cp($(doc_src)    $(ctan_root))
	cp($(manual_pdf) $(ctan_root))
	cp($(ifc_src)    $(ctan_root))
	cp($(meta_files) $(ctan_root))
	cd $(build_root) && zip -9 -r ./$(ctan_zipfile) $(name) $(tds_zipfile)

.DEFAULT: usage

