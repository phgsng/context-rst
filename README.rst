=======================================================================
                            General Advice
=======================================================================

Read the section *Features Not Implemented* in the manual first.

*Note*: if you happen to read this on BitBucket_, the error messages
some lines below just mean that *their* implementation doesn’t
understand |TeX| directives.  Don’t worry, |rstcontext| does.

=======================================================================
                             Installation
=======================================================================
The stable version -- with ConTeXt Standalone
*********************************************
An up to date version of the module is installed along with the ConTeXt
distribution (*Standalone*, formerly known as *The Minimals*).
The module should be working out of the box if you installed from the
official source_ and specified the parameter ``--modules=all``.

.. _source: http://wiki.contextgarden.net/ConTeXt_Standalone#Installing_third_party_modules

The development version -- from the Mercurial repository
********************************************************
The module package is built using omake_ which you can install via your
distribution’s package manager or through OPAM_.

::

    opam install omake

Now retrieve the repository using Mercurial:

::

    hg clone https://bitbucket.org/phg/context-rst

You should end up with a directory ``context-rst`` containing the
bare source code.
In order to build the manual, you already need |rstcontext| installed
-- a minor inconvenience that may vanish in a future release.
Assuming you have downloaded the module as part of your distribution
-- see the previous section for details --, you can now build the
package using these commands:

::

    cd context-rst
    omake tds

This will build the manual and package a zipball in the ``build/``
subdirectory that you can extract directly into your Context TEXMF.
If you are curious about the repository then issue

::

    omake usage

to print a list of available targets.

.. _omake: http://omake.metaprl.org/index.html
.. _OPAM:  http://opam.ocamlpro.com/

Manual installation from the packaged zipballs
**********************************************
The source repository can be downloaded as a ``.zip`` archive from
BitBucket_ (see the *downloads* section).
This zipball extracts into a TDS_ compliant directory tree that can be
extracted directly into the Context TEXMF.
For example, if your Context distribution is located in ``~/context``,
then unzip the archive into your local TEXMF like so:

::

    cd ~/context/tex/texmf-local/
    unzip /path/to/zipball.zip
    context --generate

The zipballs are usually provided only for the releases that are
uploaded to CTAN and the Context Garden.

.. _TDS: http://tug.org/tds/tds.html

=======================================================================
                                 Usage
=======================================================================

As |rstcontext| was developed on *texlua*, you may expect it to run
best with this Lua interpreter. For now the script accepts two command
line arguments: the input file and the output file. The following
snippet will demonstrate the main usage in a separate directory.
Assuming the contents of the TDS zipball were extracted to the current
directory you can build the documentation manually like so:

::

    mkdir tmp; cd tmp
    mtxrun --script rst --if=../doc/context/third/rst/documentation.rst --of=doc.tex
    context ../doc/context/third/rst/manual.tex

Alternatively, if you downloaded the Mercurial repository, simply issue

::

    mkdir tmp; cd tmp
    mtxrun --script rst --if=../doc/documentation.rst --of=doc.tex
    context ../doc/manual.tex

If |rstcontext| is currently not installed in your Context tree, then
you can just link the required file to the ``tmp`` directory:

::

    ln -s ../src/* .

Now the documentation should build regardless.

This should have gotten you the manual converted to a file ``doc.tex``
which was subsequently included by ``manual.tex`` which was run through
|CONTEXT| to typeset the final ``manual.pdf``. You will find extended
instructions on how to build the manual with some cosmetic improvements
in the section *Usage* of the manual itself.

Also Part of the package is a small module ``t-rst.mkiv`` which permits
direct typesetting of reStructuredText markup in |CONTEXT| MkIV source
files. For usage examples see the files ``hybridtest.tex`` and
``moduletest.tex`` in the ``doc`` subdirectory.

=======================================================================
                                License
=======================================================================

|rstcontext| is free software and may be redistributed under the terms
of the 2-clause BSD license. You should have recieved a copy of this
license in a file named “COPYING” in the source tree. If there is no
such file then please contact the maintainer_ (Contact_).

=======================================================================
                                Contact
=======================================================================

|rstcontext| was written by Philipp Gesang, ``phg42.2a`` at ``gmail``
dot ``com``.

.. |rstcontext| ctx:: {{\em rst}\kern.5pt\CONTEXT}
.. |TeX| ctx:: {\TEX}
.. |CONTEXT| ctx:: {\CONTEXT}

.. _Contact: phg42.2a@gmail.com
.. _BitBucket:  http://bitbucket.org/phg/context-rst
.. _maintainer: Contact_

